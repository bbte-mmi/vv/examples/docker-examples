# Docker: Python hello world

Create a Hello World Docker image using the [python](https://hub.docker.com/_/python/) base image. Relevant commands:

- `docker build` - creates an image based on a `Dockerfile`
- `docker tag` - add a tag to an already existing image
- `docker login` - login to Docker Hub
- `docker push` - push new image to destination (default: Docker hub)

## Building the image

We can build the image without a tag and add a tag later:

```bash
cd ex2
# build without tag
docker build .
# list images
docker image ls
  REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
  <none>              <none>              888d2bf44242        5 seconds ago       82.5 MB
# retag new image
docker tag 888d2bf44242 python-helloworld:v1
```

## Building with a tag

But usually we directly tag the image:

```bash
# build with tag
docker build -t python-helloworld:v2 .
# list images
docker image ls
  REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
  python-helloworld   v2                  888d2bf44242        2 minutes ago       82.5 MB
```

## Running a container based on the image

```bash
# run interactive and removing the container
docker run -it --rm python-helloworld:v2
```

## Pushing the image to the Docker Hub

For the Docker Hub, the image must have the name in the shape of `<username>/<imagename>:<optionalTag>`

For any other Docker registry, the image must have the format `<registryUrl>/<nameDictatedByRegistry>`. We will see this later.

```bash
# login to the Docker hub
docker login
# build or tag the image to make sure it has a correct name
docker tag python-helloworld:v2 docker.io/csabasulyok/python-helloworld:latest
# push image to the hub
docker push docker.io/csabasulyok/python-helloworld:latest
```
