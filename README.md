# Docker

Useful links:
* [Dockerfile reference](https://docs.docker.com/engine/reference/builder/) - main reference for Dockerfile structure
* [TutorialsPoint tutorial](https://www.tutorialspoint.com/docker/) - more basic tutorial
* [Romin Irani's tutorial](https://rominirani.com/docker-tutorial-series-a7e6ff90a023) - very detailed, advanced use-cases
* [Docker Compose Tutorial](https://blog.codeship.com/orchestrate-containers-for-development-with-docker-compose/)
* [Compose file reference](https://docs.docker.com/compose/compose-file/)


# Setup exercise

Try out the following Docker commands using the [ubuntu](https://hub.docker.com/_/ubuntu/) Docker image:

## Image commands

- `docker image pull` - pulls image from Docker Hub
- `docker image ls` - list images available on local machine
- `docker image rm` - remove local image

## Container commands

- `docker run` - creates & launches new container based on image
- `docker container ps` - show started containers, `-a` for all
- `docker container rm` - removes container
- `docker container logs` - removes container


# Exercises (in order of complexity)

- [Python Hello World console app](python-helloworld)
- [Node & express server](node-express-server)
- [Multistage build: Gradle & Java](spring-gradle-multistage)
- [MongoDB + node orchestration w/ compose](mongo-node-compose)
