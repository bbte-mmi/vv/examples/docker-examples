# Docker compose example

Connect multiple containers via [Docker Compose](https://docs.docker.com/compose/).

In this case, create a simple node.js server with a MongoDB database.

## Trying out node application

```bash
# start server, make sure mongo is available on localhost
npm install
node index.js

# retrieve documents
curl -i http://localhost:8080/documents

# add document
curl -i -X POST http://localhost:8080/documents \
  --header 'Content-Type: application/json' \
  --data-raw '{"name": "My Name", "age": 42}'
```

## Trying out docker-compose

```bash
# build compose
docker compose build

# run all involved services
docker compose up

# run only one service (dependencies apply)
docker compose up mongo

# run detached (console control returns to user after startup)
docker compose up -d

# stop all involved services
docker compose down
```
