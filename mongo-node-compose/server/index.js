import express from 'express';
import { MongoClient } from 'mongodb';

const app = express();
const port = process.env.PORT || 8080;

// connect to MongoDB
const mongoURL = process.env.MONGO_URL || 'mongodb://127.0.0.1';
const client = new MongoClient(mongoURL);
await client.connect();
const db = client.db('example');
const collection = db.collection('documents');
console.log(`Connected to MongoDB on ${mongoURL}`);

// GET requests return the content of the DB
app.get('/documents', async (req, res) => {
  const result = await collection.find({}).toArray();
  res.send(result);
});

// POST requests insert a new entry into the DB
app.post('/documents', express.json(), async (req, res) => {
  await collection.insertOne(req.body);
  res.send('Inserted successfully');
});

app.listen(port, () => {
  console.log(`Mongo+Node listening on http://localhost:${port}`);
});

process.on('SIGTERM', async () => {
  console.log('Closing mongo connection');
  await client.close();
  process.exit(1);
});

process.on('SIGINT', async () => {
  console.log('Closing mongo connection');
  await client.close();
  process.exit(1);
});
